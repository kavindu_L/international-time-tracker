from tkinter import *
from tkinter import ttk
import datetime
import pytz

root = Tk()
root.title("International Time")
root.geometry("300x80")
root.config(padx=5, pady=5)

def displayTime(*args):
    time_zone_tzinfo = pytz.timezone(time_zone.get())
    time_now = datetime.datetime.now().astimezone(time_zone_tzinfo)
    time_string = time_now.strftime('%d, %B %Y  %H:%M:%S')
    Label(root, text=time_string, font="TkCaptionFont").grid(row=1, column=0, columnspan=2, padx=5, pady=5)

time_zone_label = Label(root, text="Time Zone = ")
time_zone = StringVar()
time_zone_list = ttk.Combobox(root, textvariable=time_zone)
time_zone_list['values'] = pytz.all_timezones
time_zone_list.bind("<<ComboboxSelected>>", displayTime)

time_zone_label.grid(row=0, column=0, padx=5, pady=5)
time_zone_list.grid(row=0, column=1, padx=5, pady=5)


root.mainloop()
